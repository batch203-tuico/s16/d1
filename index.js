// console.log("Hello, world!");

// [SECTION] Arithmetic Operators

	let x = 101;
	let y = 25;

	// Addition
	let sum = x + y;
	console.log("Result of addition operator: " +sum);

	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " +difference);

	// Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " +product);

	// Division
	let quotient = x / y;
	console.log("Result of division operator: " +quotient);

	// Modulo -> Remainder of the division operator (used to get odd/even number)
	let modulo = x % y;
	console.log("Result of modulo operator: " +modulo);

// [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the "right hand" operand to a variable.
	let assignmentNumber = 8;
	console.log("The current value of the assignmentNumber variable: " +assignmentNumber);

	// Addition Assignment Operator
				// 8 + 2
	// assignmentNumber = assignmentNumber + 2; long method
	assignmentNumber += 2; //shorthand method
	console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

	// Subtraction/Multiplication/Division (-=, *=, /=)
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " +assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " +assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " +assignmentNumber);


// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis
// When multiple operators are applied in a single statement, it follows PEMDAS.

/*
	The operation were done in the following orders
	1. 3 * 4 = 12 | 1 + 2 - 12 / 5
	2. 12 / 5 = 2.4 | 1 + 2 - 2.4
	3. 1 + 2 = 3 | 3-2.4
	4. 3 - 2.4 = 0.6 //result
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " +mdas);


/*
	The operations were done in the following order:
	1. 4 / 5 = 0.8 | 1 + (2 - 3) * 0.8
	2. 2 - 3 = -1 | 1 + (-1) * 0.8
	3. (-1) * 0.8 = (-0.8) | 1 - 0.8
	4. 1 - 0.8 = 0.20 //result
*/
// let pemdas = 1 + (2 - 3) * (4 / 5); //0.20
//  grouping the operators using parenthesis will deliver different result.
let pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " +pemdas); //result is 0


// [SECTION] Increment and Decrement
// Operators that add or subtract a value by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

	let z = 1;
	// The value of "z" is added by a value of one before returning the value and storing it in the variable increment.
					// 1 + 1 = 2
	let increment = ++z;
	console.log("Result of pre-increment: " +increment);
	console.log("Result of pre-increment for z: " +z);
	// Javascript read the code from top to bottom and left to right 
	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one.
	increment = z++;
	// The value of "z" is at 2 before it was incremented.
	console.log("Result of post-increment: " +increment);
	console.log("Result of post-increment for z: " +z);


	let decrement = --z;
	console.log("Result of pre-decrement:" +decrement);
	console.log("Result of pre-decrement for z: " +z);

	decrement = z--;
	console.log("Result of post-decrement: " +decrement);
	console.log("Result of post-decrement for z: " +z);


// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another.

	let numA = '10';
	let numB = 12;

	// Coercion values usually performs concatentaion if a string value is involved.
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoersion = numC + numD;
	console.log(nonCoersion); //30
	console.log(typeof nonCoersion);


	/*
		- The result is a number
		- The boolean "true" is also associated with the value of 1.
	*/

	let numE = true + 1;
	console.log(numE);

	/*
		- The result is still a number
		- The boolean "false" is also associated with the value of 0.
	*/

	let numF = false +1;
	console.log(numF);


// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands/
// After evaluation, it returns a boolean value.

	let juan = "juan";

	// Equality Operator (==)
	/*
		- Checks whether the operands are equal/have the same content.
		- Attempt to CONVERT and COMPARE operands of different data type.
	*/
	console.log(1 == 1);  //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	// Compare two strings that are the same.
	// Case sensitive
	console.log('juan' == 'juan'); //true
	// Compares a string with the variable juan
	console.log('juan' == juan); //true


	// Inequality Operator (!=) not equal
	/*
		- Checks whether the operands are not equal/have the different value.
		- Attempts to CONVERT and COMPARE operand of different data type.
	*/
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(0 != false); //false
	console.log('juan' != 'juan'); //false
	console.log('juan' != juan); //false


	// Strict Equality Operator (===)
	// Check whether the operands are equal or have the same conntent.
	// Also COMPARES the DATA TYPES of 2 values.
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(0 === false); //false
	console.log('juan' === 'juan'); //true
	console.log('juan' === juan); //true


	// Strict Inequality Operator (!==)
	/*
		- Checks whether the operands are not equal/don't have the same content.
		- Also COMPARES the DATA TYPES of 2 values.
	*/
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true
	console.log('juan' !== 'juan') //false
	console.log('juan' !== juan); //false


// [SECTION] Relational Operators
// Some comparison operators check whether one values is greater or less than to the other value.

let a = 50;
let b = 65;

	// GT or Greater Than Operator (>)
	let isGreaterThan = a > b; // 50 > 65 = false
	console.log(isGreaterThan); //false

	// LT or Less Than Operator (<)
	let isLessThan = a < b; // 50 < 65 = true
	console.log(isLessThan); // true

	// GTE or Greater Than or Equal (>=)
	let isGTOrEqual = a >= b; // 50>= 65 = false
	console.log(isGTOrEqual); //false

	// LTE or Less Than or Equal (<=)
	let isLTOrEqual = a <= b; // 50 <= 65 = true
	console.log(isLTOrEqual); //true

	let numStr = '30';
	console.log(a > numStr); //true -forced coercion to change the string to number.

	let str = 'twenty';
	console.log(b >= str); //false - since the string is not numeric, the string was not converted to a number. 65 >= NaN (Not a Number);


// [SECTION] Logical Operators
// Logical Operators allows us to be more specific in the logical combination of conditions and evaluations. It return boolean.

let isLegalAge = true;
let isRegistered = false;

	// Logical AND Operator (&& - Double Ampersand)
	// Returns true if all operands are true.
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " +allRequirementsMet);


	// Logical OR Operator (|| - Double Pipe)
	// Return true if one the operand is true.
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " +someRequirementsMet);


	// Logical NOT Operator (! - Exclamation Point);
	// Returns the opposite value
	let someRequirmentsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " +someRequirmentsNotMet);